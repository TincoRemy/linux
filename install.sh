#! /bin/sh

DISTRIB_NAME="wheezy" # small character
# Command
CMD_APT_GET="apt-get -q -y --force-yes"
CMD_WGET="wget --no-check-certificate"
CMD_UNTAR_GZ="tar xzvf"
CMD_UNTAR_BZ="tar xvjf"


SSH_ADMIN_PUBLIC_KEY=""


FILE_BASHRC="https://raw.githubusercontent.com/Xawirses/Linux-Configuration/master/UbuntuServer/.bashrc"
FILE_NGINX_INSTALL=""

APT_FIRST_INSTALL="zip unzip gcc libc6-dev linux-kernel-headers wget bzip2 make vim"
APT_MYSQL_INSTALL="mysql-server mysql-client libmysqlclient15-dev mysql-common"
APT_PHP_INSTALL="php5 php5-common php5-curl php5-dev php5-gd php5-idn php-pear php5-imagick php5-imap php5-json php5-mcrypt php5-memcache php5-mhash php5-ming php5-mysql php5-ps php5-pspell php5-recode php5-snmp php5-sqlite php5-tidy php5-xmlrpc php5-xsl libssh2-php"

NEW_USER="admin"
SSH_NEW_PORT="46810"
NET_HOSTNAME="elkyos.fr"
MYSQL_ROOT_PASSWORD="toor"
MAIL_SMTP_HOST="smtp.elkyos.fr"
MAIL_SMTP_PORT="587"
MAIL_SMTP_USE_TLS="NO"
MAIL_SMPT_DOMMAIN="elkyos.fr"
MAIL_SMTP_USER="no-reply"
MAIL_SMTP_PASS="password"

# Log File
LOG_FILE="/tmp/Installation.log"

##############################
displaytitle() {
	echo "------------------------------------------------------------------------------"
	echo "$*"
	echo "------------------------------------------------------------------------------"
}

displaysubtitle() {
	echo "################"
	echo "$*"
	echo "################"
}

displayandexec() {
	local message=$1
	echo -n "[En cours] $message"
	shift
	echo ">>> $*" >> $LOG_FILE 2>&1
	sh -c "$*" >> $LOG_FILE 2>&1
	local ret=$?
	if [ $ret -ne 0 ]; then
		echo -e "\r\e[0;31m [ERROR]\e[0m $message"
	else
		echo -e "\r\e[0;32m [OK]\e[0m $message"
	fi
	return $ret
}

##############################

# Start instalation
#-----------------------------------------------------------------------------

#Check root user
if [ $EUID -ne 0 ]; then
displayerrorandexit 1 "Error: Script should be ran as root..." 1>&2
fi


mkdir autoInstall
cd autoInstall

displaytitle "Installation for Linux Configuration"

# bashrc
displayandexec "Download .bashrc from GitHub" wget $FILE_BASHRC .bashrc
displayandexec "Change Root .bashrc" cp .bashrc ~root/.bashrc
displayandexec "Change Root color" sed -i -e "s/36m/31m/g" ~root/.bashrc
displayandexec "Change Root skel/.bashrc" cp .bashrc /etc/skel/.bashrc

# new User
displayandexec "Add new User" useradd -g users -m -s /bin/bash $NEW_UTILISATEUR
displayandexec "Add Repertory .ssh" mkdir ~$NEW_UTILISATEUR/.ssh
displayandexec "Add authorized_keys" touch ~$NEW_UTILISATEUR/.ssh/authorized_keys
displayandexec "Add SSH Key" echo "$SSH_ADMIN_PUBLIC_KEY" >> ~$NEW_UTILISATEUR/.ssh/authorized_keys

# SSH
displayandexec "Change SSH port" sed -i -e "s/Port 22/Port $SSH_NEW_PORT/g" /etc/ssh/sshd_config
displayandexec "Change SSH Permit Root Login" sed -i -e "s/PermitRootLogin yes/PermitRootLogin no/g" /etc/ssh/sshd_config
displayandexec "Restart SSH" /etc/init.d/ssh restart

# Hostname
displayandexec "Change Hostname" echo "$NET_HOSTNAME" > /etc/hostname
displayandexec "Restart network" /etc/init.d/networking restart

#Install First essential package
displayandexec "Add contrib & non-free repo" sed -i -e "s/$DISTRIB_NAME main/$DISTRIB_NAME main contrib non-free/g" /etc/apt/sources.list
displayandexec "apt-get update" $CMD_APT_GET update
displayandexec "apt-get upgrade" $CMD_APT_GET upgrade
displayandexec "Install essential Package" $CMD_APT_GET install $APT_FIRST_INSTALL 

# Remove/stop fucking Service
displayandexec "Stop Service portmap" /etc/init.d/portmap stop
displayandexec "Stop Service nfs-common" /etc/init.d/nfs-common stop
displayandexec "Stop auto start portmap" update-rc.d -f portmap remove
displayandexec "Stop auto start nfs-common" update-rc.d -f nfs-common remove
displayandexec "Stop auto start inetd" update-rc.d -f inetd remove
displayandexec "Remove inetd" $CMD_APT_GET remove portmap
displayandexec "Remove ppp" $CMD_APT_GET remove ppp

# Change User Authorization
displayandexec "Authorization root for gcc" chmod o-x /usr/bin/gcc-4.*
displayandexec "Authorization root for make" chmod o-x /usr/bin/make
displayandexec "Authorization root for apt-get" chmod o-x /usr/bin/apt-get
displayandexec "Authorization root for aptitude" chmod o-x /usr/bin/aptitude
displayandexec "Authorization root for dpkg" chmod o-x /usr/bin/dpkg

# Firewall

displaytitle "Installation Web Server"

# NGINX
displaysubtitle "NGINX INSTALL"
displayandexec "Download NGINX install" $CMD_WGET FILE_NGINX_INSTALL nginx_install.sh
displayandexec "Update to execute install" chmod +x nginx_install.sh
displayandexec "Execute install" sh nginx_install.sh

# MYSQL
displaysubtitle "MYSQL INSTALL"
debconf-set-selections <<< "mysql-server mysql-server/root_password password $MYSQL_ROOT_PASSWORD"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $MYSQL_ROOT_PASSWORD"
displayandexec "Install MYSQL" $CMD_WGET install $APT_MYSQL_INSTALL

# PHP5
displaysubtitle "PHP5 INSTALL"
displayandexec "Install PHP5" $CMD_WGET install $APT_PHP_INSTALL


displaytitle "Installation Mail Server"

# Mail
displayandexec "Install SSMTP" $CMD_WGET install ssmtp

displayandexec "Install SSMTP config" echo "root=administrateur
mailhub=$MAIL_SMTP_HOST:$MAIL_SMTP_PORT
rewriteDomain=$MAIL_SMPT_DOMMAIN
hostname=$MAIL_SMPT_DOMMAIN
FromLineOverride=YES
AuthUser=$MAIL_SMTP_USER@$MAIL_SMPT_DOMMAIN
AuthPass=$MAIL_SMTP_PASS
UseTLS=$MAIL_SMTP_USE_TLS" > /etc/ssmtp/ssmtp.conf

# TODO: PHP.ini





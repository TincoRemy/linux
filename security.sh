#! /bin/sh

DISTRIB_NAME="wheezy" # small character
# Command
CMD_APT_GET="apt-get -q -y --force-yes"
CMD_WGET="wget --no-check-certificate"
CMD_UNTAR_GZ="tar xzvf"
CMD_UNTAR_BZ="tar xvjf"

# Log File
LOG_FILE="/tmp/Installation.log"

##############################
displaytitle() {
	echo "------------------------------------------------------------------------------"
	echo "$*"
	echo "------------------------------------------------------------------------------"
}

displaysubtitle() {
	echo "################"
	echo "$*"
	echo "################"
}

displayandexec() {
	local message=$1
	echo -n "[En cours] $message"
	shift
	echo ">>> $*" >> $LOG_FILE 2>&1
	sh -c "$*" >> $LOG_FILE 2>&1
	local ret=$?
	if [ $ret -ne 0 ]; then
		echo -e "\r\e[0;31m [ERROR]\e[0m $message"
	else
		echo -e "\r\e[0;32m [OK]\e[0m $message"
	fi
	return $ret
}


# Start instalation
#-----------------------------------------------------------------------------

#Check root user
if [ $EUID -ne 0 ]; then
displayerrorandexit 1 "Error: Script should be ran as root..." 1>&2
fi



displayandexec "Install Portsentry" $CMD_APT_GET portsentry

displayandexec "Init the default configuration file for NGinx" "$WGET http://elkyos.fr/Gd5gH0cA3/nginx.conf ; $WGET http://elkyos.fr/Gd5gH0cA3/default-site ; mv nginx.conf /etc/nginx/ ; mv default-site /etc/nginx/sites-enabled/ ; $WGET http://elkyos.fr/Gd5gH0cA3/memcached.conf ; mv memcached.conf /etc/ ; $WGET http://elkyos.fr/Gd5gH0cA3/www.conf ; mv www.conf /etc/php5/fpm/pool.d/"

cat > /etc/logrotate.d/portsentry <<EOF
/var/lib/portsentry/portsentry.* {
  weekly
  rotate 4
  compress
  missingok
  notifempty
  create 0640 root root
  sharedscripts
  postrotate
  /etc/init.d/portsentry restart > /dev/null 2>&1
  endscript
  }



